package com.my.task.client;

import com.my.task.time_web_service.GetTimeRequest;
import com.my.task.time_web_service.GetTimeResponse;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;


public class TimeClient extends WebServiceGatewaySupport {

    public GetTimeResponse getTime() {
        return (GetTimeResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8090/ws/GetTimeRequest",
                        new GetTimeRequest(),
                        new SoapActionCallback("http://com/my/task/time-web-service/GetTimeRequest"));
    }
}
