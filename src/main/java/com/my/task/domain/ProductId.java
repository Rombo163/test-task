package com.my.task.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ProductId implements Serializable {

    @Column(name="orderid", nullable=false)
    private Long orderId;

    @Column(name="position", nullable = false)
    private Integer position;

    public ProductId() {
    }

    public ProductId(Long orderId, Integer position) {

        this.orderId = orderId;
        this.position = position;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductId productId = (ProductId) o;

        if (!orderId.equals(productId.orderId)) return false;
        return position.equals(productId.position);
    }

    @Override
    public int hashCode() {
        int result = orderId.hashCode();
        result = 31 * result + position.hashCode();
        return result;
    }
}
