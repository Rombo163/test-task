package com.my.task.domain;

import javax.persistence.*;

/**
 * Product entity.
 *
 * @author Lisitsyn Roman
 * @version 1.0
 */

@Entity
@Table(name="products")
public class Product {

    @EmbeddedId
    private ProductId id;

    @Column(name="serialnumber")
    private String serialNumber;

    @Column(name="name")
    private String name;

    @Column(name="amount")
    private Integer amount;

    @MapsId("orderId")
    @ManyToOne
    @JoinColumn(name="orderid", referencedColumnName = "id")
    private Order order;

    public ProductId getId() {
        return id;
    }

    public void setId(ProductId id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (id != null ? !id.equals(product.id) : product.id != null) return false;
        if (!serialNumber.equals(product.serialNumber)) return false;
        return name.equals(product.name);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + serialNumber.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}
