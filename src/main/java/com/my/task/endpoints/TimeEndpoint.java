package com.my.task.endpoints;

import com.my.task.time_web_service.GetTimeResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.GregorianCalendar;


@Endpoint
public class TimeEndpoint {

    private static final Logger log = LoggerFactory.getLogger(TimeEndpoint.class);

    private static final String NAMESPACE_URI = "http://com/my/task/time-web-service";

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getTimeRequest")
    @ResponsePayload
    public GetTimeResponse getTime() throws DatatypeConfigurationException {
        GetTimeResponse response = new GetTimeResponse();
            response.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
        return response;
    }
}
