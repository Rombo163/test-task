package com.my.task.controller;

import com.my.task.client.TimeClient;
import com.my.task.domain.Order;
import com.my.task.persistence.OrderDao;
import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.annotation.RequestAction;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.ocpsoft.rewrite.faces.annotation.Deferred;
import org.ocpsoft.rewrite.faces.annotation.IgnorePostback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Scope(value="session")
@Component(value="orderList")
@ELBeanName(value="orderList")
@Join(path="/", to="order-list.jsf")
public class OrderListController {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private TimeClient timeClient;

    private List<Order> orders;

    @Deferred
    @RequestAction
    @IgnorePostback
    public void loadData() {
        orders = orderDao.findAll(new Sort(Sort.Direction.ASC, "id"));
    }

    public List<Order> getOrders() {
        return orders;
    }

    public Date getDate() {
        return timeClient.getTime().getDate().toGregorianCalendar().getTime();
    }
}
